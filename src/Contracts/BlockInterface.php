<?php

namespace Fantassin\Core\WordPress\Contracts;

interface BlockInterface
{
    public function getName(): string;
}
